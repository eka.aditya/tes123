window.fbAsyncInit = () => {
  FB.init({
    appId      : '426547564455641',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.12'
  });

(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function facebookLogin(){
     FB.login(function(response){
       console.log(response);
     }, {scope:'public_profile'})
   }
