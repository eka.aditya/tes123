var input = document.getElementById("messageInput");
var i;
var existingchat = JSON.parse(localStorage.getItem("allchat"));

input.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
      var existingchat = JSON.parse(localStorage.getItem("allchat"));
      if(existingchat == null) existingchat = [];
      localStorage.setItem("input", input.value);
      existingchat.push(input.value);
      localStorage.setItem("allchat", JSON.stringify(existingchat));
      var para = document.createElement("p");
      para.classList.add("msg-send");
      var node = document.createTextNode(input.value);
      para.appendChild(node);
      var element = document.getElementById("input");
      element.appendChild(para);
      input.value = '';
    }
});

/* load chat yang ada di localStorage biar tampil di chat*/
for (i = 0; i < existingchat.length; i++) {
  var para = document.createElement("p");
  para.classList.add("msg-send");
  var node = document.createTextNode(existingchat[i]);
  para.appendChild(node);
  var element = document.getElementById("input");
  element.appendChild(para);
};

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

var themes = {
  "results": [
    {"id":1,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":2,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":3,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":4,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":5,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":6,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":7,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":8,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":9,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":10,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":11,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
],
  "pagination": {"more": true}
};

var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

localStorage.setItem('themes', JSON.stringify(themes));

var retrievedObject = localStorage.getItem('themes');

$(document).ready(function() {
    $('.my-select').select2({
      'data': JSON.parse(retrievedObject)
    })
    $('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select

    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada

    // [TODO] ambil object theme yang dipilih

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya

    // [TODO] simpan object theme tadi ke local storage selectedTheme
    })
});
