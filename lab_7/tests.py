from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import Friend
# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_lab7_friend_list_url_is_exits(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_friend_list_using_friend_list_func(self):
        found = resolve('/lab-7/friend-list/')
        self.assertEqual(found.func, friend_list)

    def test_model_can_create_object(self):
        # Creating a new input
        new_Friend = Friend.objects.create(friend_name='Mosalah', npm='1406555555')

        # Retrieving all available inputs
        counting_all_available_friends = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friends, 1)

    def test_can_add_friend(self):
        response = self.client.post('/lab-7/add-friend/', data={'name': 'De Bruyne', 'npm' : '1406555551'})
        counting_all_available_friends = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friends, 1)
        self.assertEqual(response.status_code, 200)
